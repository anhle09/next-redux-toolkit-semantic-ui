import { configureStore } from '@reduxjs/toolkit'
import { useSelector } from 'react-redux'
import rootReducer, { selector } from './reducer'

const store = configureStore({
    reducer: rootReducer,
})

export const useAuthDataSelector = () => useSelector(selector.authData)

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export default store
