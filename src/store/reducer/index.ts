import authReducer from './authSlice'

const rootReducer = {
    auth: authReducer,
}

export default rootReducer

export const selector = {
    authData: (state: any) => state.auth,
}
