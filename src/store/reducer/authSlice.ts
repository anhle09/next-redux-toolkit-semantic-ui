import { createSlice } from '@reduxjs/toolkit'

interface AuthState {
    currentUser?: {
        name: string
        role: string
    }
}

const initialState: AuthState = {

}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login(state, action) {
            state.currentUser = action.payload
        },
        logout(state) {
            state.currentUser = undefined
        },
    },
})

export const authAction = authSlice.actions

export default authSlice.reducer
