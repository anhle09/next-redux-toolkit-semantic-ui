import { Provider } from 'react-redux'
import { render, fireEvent, screen, waitFor } from '@testing-library/react'
import configureMockStore from 'redux-mock-store'
import LoginPage from '@/pages/login'

jest.mock('next/router', () => require('next-router-mock'))
const mockStore = configureMockStore()

describe('LoginPage', () => {
    it('renders the login form', async () => {
        const store = mockStore({ auth: {} })
        render(
            <Provider store={store}>
                <LoginPage />
            </Provider>
        )
        expect(screen.getByText('Log-in to your account')).toBeInTheDocument()

        const emailInput = screen.getByPlaceholderText('Email')
        const passwordInput = screen.getByPlaceholderText('Password')
        const loginButton = screen.getByText('Login')

        // Not fill the form
        fireEvent.click(loginButton)
        await waitFor(() => expect(store.getActions()[0]).toBeUndefined())

        // Fill values that are too long
        fireEvent.change(emailInput, { target: { value: 'invalid email' } })
        fireEvent.change(passwordInput, { target: { value: 'a'.repeat(51) } })
        fireEvent.click(loginButton)
        await waitFor(() => expect(store.getActions()[0]).toBeUndefined())

        // Fill valid values
        fireEvent.change(emailInput, { target: { value: 'test@mail.com' } })
        fireEvent.change(passwordInput, { target: { value: '123' } })
        fireEvent.click(loginButton)
        await waitFor(() => {
            const { payload } = store.getActions()[0]
            expect(payload).toEqual({
                'email': 'test@mail.com',
                'password': '123',
            })
        })
    })
})
