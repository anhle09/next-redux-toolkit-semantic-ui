import { TextField } from '@/component/form'
import { render, fireEvent, screen, waitFor } from '@testing-library/react'
import { Form, Formik } from 'formik'
import * as yup from 'yup'

const schema = yup.object().shape({
    fieldName: yup.string().required('Required').max(10, 'Max length')
})

describe('TextField', () => {
    it('render TextField', async () => {
        const submitFn = jest.fn()
        render(
            <Formik
                initialValues={{ fieldName: '' }}
                validationSchema={schema}
                onSubmit={submitFn}
            >
                <Form>
                    <TextField
                        name='fieldName'
                        placeholder='Placeholder'
                    />
                    <button type='submit'>Submit</button>
                </Form>
            </Formik>
        )

        const textField = screen.getByPlaceholderText('Placeholder')
        const submitBtn = screen.getByText('Submit')

        // Not fill the form
        fireEvent.click(submitBtn)
        await waitFor(() => {
            expect(textField).toBeInTheDocument()
            expect(screen.getByText('Required')).toBeInTheDocument()
            expect(submitFn).not.toBeCalled()
        })

        // Fill values that are too long
        fireEvent.change(textField, { target: { value: 'a'.repeat(11) } })
        fireEvent.click(submitBtn)
        await waitFor(() => {
            expect(screen.queryByText('Required')).toBeNull()
            expect(screen.getByText('Max length')).toBeInTheDocument()
            expect(submitFn).not.toBeCalled()
        })

        // Fill valid values
        fireEvent.change(textField, { target: { value: 'abc' } })
        fireEvent.click(submitBtn)
        await waitFor(() => {
            expect(screen.queryByText('Max length')).toBeNull()
            expect(submitFn).toBeCalled()
        })
    })
})
