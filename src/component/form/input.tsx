import { Field, FieldProps } from 'formik'
import { Form } from 'semantic-ui-react'

type TextFieldProps = {
    name: string
    type?: string
    placeholder?: string
    className?: string
}

export function TextField({ name, className, ...rest }: TextFieldProps) {
    return (
        <Field name={name}>
            {({ field, meta }: FieldProps) => (
                <div className={className}>
                    <Form.Input
                        fluid
                        {...field}
                        {...rest}
                        error={meta.error && meta.touched}
                    />
                    {meta.error && meta.touched && (
                        <span className='text-red'>
                            {meta.error}
                        </span>
                    )}
                </div>
            )}
        </Field>
    )
}
