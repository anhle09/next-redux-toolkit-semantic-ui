export const message = {
    REQUIRED: 'Field required',
    OVER_CHARACTER: 'Over characters',
    INVALID_EMAIL: 'Invalid email',
}
