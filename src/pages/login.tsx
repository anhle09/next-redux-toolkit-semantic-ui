import { useDispatch } from 'react-redux'
import { useRouter } from 'next/router'
import { Button, Segment } from 'semantic-ui-react'
import { Form, Formik } from 'formik'
import * as yup from 'yup'
import { authAction } from '@/store/reducer/authSlice'
import { TextField } from '@/component/form'
import { message as MSG } from '@/utils/message'

const loginSchema = yup.object().shape({
    email: yup.string()
        .email(MSG.INVALID_EMAIL)
        .required(MSG.REQUIRED)
        .max(50, MSG.OVER_CHARACTER),
    password: yup.string()
        .required(MSG.REQUIRED)
        .max(50, MSG.OVER_CHARACTER),
})

export default function LoginPage() {
    const dispatch = useDispatch()
    const router = useRouter()

    const handleLogin = async (formValue: any) => {
        dispatch(authAction.login(formValue))
        router.push('/')
    }

    return (
        <div className='box-center h-100'>
            <div className='w-100' style={{ maxWidth: 480 }}>
                <h2 className='text-center'>
                    Log-in to your account
                </h2>
                <Segment stacked className='pa-8'>
                    <Formik
                        initialValues={{ email: '', password: '' }}
                        validationSchema={loginSchema}
                        onSubmit={handleLogin}
                    >
                        <Form>
                            <label className='grid-col-3'>
                                <div className='py-2'>Email address *</div>
                                <TextField
                                    className='col-span-2'
                                    name='email'
                                    placeholder='Email'
                                />
                            </label>
                            <label className='grid-col-3 mt-2'>
                                <div className='py-2'>Password *</div>
                                <TextField
                                    className='col-span-2'
                                    name='password'
                                    type='password'
                                    placeholder='Password'
                                />
                            </label>
                            <Button
                                fluid
                                type='submit'
                                className='bg-primary mt-4'
                            >
                                Login
                            </Button>
                        </Form>
                    </Formik>
                </Segment>
            </div>
        </div>
    )
}
